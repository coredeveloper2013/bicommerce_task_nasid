<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ProductController extends Controller
{
    public function index()
    {

        return response()->json('Product Controller', 200);
        
    }

    public function getBigCommerceProducts()
    {

        $client = new Client(['verify' => false ]);

        $response = $client->request('GET', 'https://api.bigcommerce.com/stores/lj4x1qpr56/v3/catalog/products', [
            'headers' => [
                'X-Auth-Token'  => config('services.big_commerce.app_scopes'),
                'X-Auth-Client' => config('services.big_commerce.client_id'),
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json'
            ]
        ]);

        $responseData = json_decode($response->getBody(), true);

        $products = $responseData['data'];
        $productCount = count($responseData['data']);
        $meta = $responseData['meta'];

        return response()->json(['success' => true, 'message' => 'Getting all big commerce products.', 'productCount' => $productCount, 'products' => $products, 'meta' => $meta]);

    }

    public function getShopifyProducts()
    {

        $client = new Client(['verify' => false ]);
        $shopifyProductsApiUrl = 'https://nasid112.myshopify.com/admin/api/2020-07/products.json?limit=250&page=1';
        
        $response = $client->request('GET', $shopifyProductsApiUrl);

        $responseData = json_decode($response->getBody(), true);

        dd($responseData);

        return response()->json(['success' => true, 'message' => 'Getting all shopify products.', 'products' => array()]);

    }

}

?>