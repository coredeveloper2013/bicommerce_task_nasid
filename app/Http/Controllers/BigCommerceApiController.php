<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class BigCommerceApiController extends BaseController
{

    private function getApiClientId() {
        
        return env('BC_API_ACCOUNT_CLIENT_ID');

    }

    private function getApiSecret(Request $request) {
        
        return env('BC_API_ACCOUNT_SECRET');
        
    }

    private function getApiAccessToken(Request $request) {
        
        return env('BC_API_ACCOUNT_ACCESS_TOKEN');
        
    }

    private function getApiStoreHash(Request $request) {
        
        return env('BC_API_ACCOUNT_STORE_HASH');
        
    }


    public function install(Request $request)
    {

        return 'Install BigCommerce App...';

    }

    public function load(Request $request)
    {

        return 'Load BigCommerce App...';

    }

    public function getProducts(Request $request)
    {
        $page = $request->page ?? 1;
        $perPage = $request->perPage ?? 10;
        //dd($perPage);

        $endPoint = 'v3/catalog/products';

        $accessToken = $this->getApiAccessToken($request);
        $clientId = $this->getApiClientId($request);
        $storeHash = $this->getApiStoreHash($request);

        $apiUrl = 'https://api.bigcommerce.com/' . $storeHash . '/' . $endPoint;
        //return $apiUrl;

        $client = new Client(['verify' => false ]);

        $response = $client->request('GET', $apiUrl, [
            'headers' => [
                'X-Auth-Token'  => $accessToken,
                'X-Auth-Client' => $clientId,
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json'
            ]
        ]);

        $responseData = json_decode($response->getBody(), true);

        $items = $responseData['data'];
        //$productCount = count($responseData['data']);
        //$meta = $responseData['meta'];

        $productCount = count($items);
        $pageCount = ceil($productCount / $perPage);
        $offset = ($page - 1) * $perPage;
        $products = array_slice($items, $offset, $perPage);

        //return response()->json([ 'success' => true, 'message' => 'Getting all big commerce products.', 'products' => $responseData ]);
        return response()->json([ 'success' => true, 'productCount' => $productCount, 'pageCount' => $pageCount, 'perPage' => $perPage, 'currentPage' => $page, 'products' => $products ]);

    }

    public function getOrders(Request $request)
    {
        $page = $request->page ?? 1;
        $perPage = $request->perPage ?? 10;

        $endPoint = 'v2/orders';

        $accessToken = $this->getApiAccessToken($request);
        $clientId = $this->getApiClientId($request);
        $storeHash = $this->getApiStoreHash($request);

        $apiUrl = 'https://api.bigcommerce.com/' . $storeHash . '/' . $endPoint;
        //return $apiUrl;
        //dd($apiUrl);

        $client = new Client(['verify' => false ]);

        $response = $client->request('GET', $apiUrl, [
            'headers' => [
                'X-Auth-Token'  => $accessToken,
                'X-Auth-Client' => $clientId,
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json'
            ]
        ]);

        $items = json_decode($response->getBody(), true);
        //$orderCount = count($orders);
        $orderCount = count($items);
        $pageCount = ceil($orderCount / $perPage);
        $offset = ($page - 1) * $perPage;
        $orders = array_slice($items, $offset, $perPage);

        //dd($orders);

        //return response()->json([ 'success' => true, 'message' => 'Getting all big commerce orders.', 'orders' => $orders ]);
        return response()->json([ 'success' => true, 'orderCount' => $orderCount, 'pageCount' => $pageCount, 'perPage' => $perPage, 'currentPage' => $page, 'orders' => $orders ]);

    }

    public function uninstall()
    {

        return 'Uninstall BigCommerce App...';

    }

}
