<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ShopifyController extends Controller
{

    protected $shopifyClient;

    /**
     * This builds our redirect URL and sends the user back
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function install(Request $request)
    {
        $api_key = config('services.shopify.client_api_key');
        $scopes = config('services.shopify.app_scopes');
        $redirect_url = "/shopify/load";
        $nonce = 1;

        //return redirect()->to("https://{$request->shop}/admin/oauth/authorize?client_id={$api}&scope={$scopes}&redirect_uri={$redirect_url}&state={$nonce}");
        return redirect()->to("https://demo0112/admin/oauth/authorize?client_id={$api_key}&scope={$scopes}&redirect_uri={$redirect_url}&state={$nonce}");
        
    }

    public function load(Request $request)
    {
        $client_id = config('services.shopify.client_api_key');
        $client_secret = config('services.shopify.client_secret');

        try {
           
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function uninstall(Request $request)
    {
        return $request;
    }

}