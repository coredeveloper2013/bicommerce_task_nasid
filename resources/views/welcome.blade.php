<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BigCommerce App</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/home.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    BigCommerce Tasks 
                </div>
                <h3>[ Assigned To: Nasid Kamal ]</h3>
                <hr>
                <br>
                <div class="links">
                    <a href="{{ route('get_big_commerce_products') }}">Check BigCommerce Products API</a>
                    <a href="{{ route('get_big_commerce_orders') }}">Check BigCommerce Orders API</a>
                    <a href="https://bitbucket.org/coredeveloper2013/bicommerce_task_nasid.git">Check BitBucket Repository</a>
                </div>
                <br>
                <div class="links">
                    <a href="{{ route('login') }}">Login</a> <b>OR</b> <a href="{{ route('register') }}">Register</a> <b>to explore BigCommerce data in Dashboard.</b>
                </div>
            </div>
        </div>
    </body>
</html>
