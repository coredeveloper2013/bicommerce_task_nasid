@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <div id="bigcommerce-app">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- JavaScript -->
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('/js/sweetalert.min.js') }}"></script>
@endsection
