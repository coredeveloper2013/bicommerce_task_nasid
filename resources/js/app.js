/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import VueRouter from "vue-router";
Vue.use(VueRouter);

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

import Home from "./components/Home";
import BigCommerceProducts from "./components/BigCommerceProducts";
import BigCommerceOrders from "./components/BigCommerceOrders";


let routes = [
    {
        path        : "/home",
        component   : Home,
        name        : 'home'
    },
    {
        path        : "/bigcommerce-products",
        component   : BigCommerceProducts,
        name        : 'BigCommerceProducts'
    },
    {
        path        : "/bigcommerce-orders",
        component   : BigCommerceOrders,
        name        : 'BigCommerceOrders'
    }

];

const router = new VueRouter({
    mode: "history",
    routes
});

export default router;

const app = new Vue({
    router,
    template: `
    <div>
    <div class="top-right links">
        <router-link to="/bigcommerce-products" class="bc-app-link">BigCommerce Products</router-link>
        <router-link to="/bigcommerce-orders" class="bc-app-link">BigCommerce Orders</router-link>
    </div>
    <br>
    <br>
    <router-view class="view"></router-view>
    </div>
    `
}).$mount('#bigcommerce-app');
