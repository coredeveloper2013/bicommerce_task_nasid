<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/big-commerce/all-products', 'ProductController@getBigCommerceProducts')->name('get_big_commerce_products');
Route::get('/big-commerce/all-orders', 'BigCommerceApiController@getOrders')->name('get_big_commerce_orders');
Route::get('/shopify/all-products', 'ProductController@getShopifyProducts')->name('get_shopify_products');

Route::group(['prefix' => 'bc-auth'], function () {

    Route::get('install', 'BigCommerceApiController@install');
    Route::get('load', 'BigCommerceApiController@load');
    Route::get('uninstall', 'BigCommerceApiController@uninstall');
    Route::post('products', 'BigCommerceApiController@getProducts');
    Route::get('orders', 'BigCommerceApiController@getOrders');

});

Route::get('/{vue_route?}', 'HomeController@index')->where('vue_route', '(.*)');

// Route::prefix('shopify')->group(function () {
//     Route::get('install', 'ShopifyController@install');
//     Route::get('load', 'ShopifyController@load');
//     Route::get('uninstall', 'ShopifyController@uninstall');
// });
